#!/bin/sh

directories="staging production"

for d in $directories; do
    rm -rf "$d-out"
    mkdir -p "$d-out"
    kustomize build \
        --load-restrictor LoadRestrictionsNone \
        --output "$d-out" \
        "$d/"
done
